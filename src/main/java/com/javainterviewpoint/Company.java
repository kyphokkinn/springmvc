package com.javainterviewpoint;

import java.io.Serializable;
import java.util.Objects;

public class Company implements Serializable {
    private static final long serialVersionUID = -1280037900360314186L;
    private Integer id;
    private String comname;
    private String comaddress;

    public Company(){super();}
    public Company(Integer id, String comname, String comaddress) {
        super();
        this.id = id;
        this.comname = comname;
        this.comaddress = comaddress;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getComname() {
        return comname;
    }

    public void setComname(String comname) {
        this.comname = comname;
    }

    public String getComaddress() {
        return comaddress;
    }

    public void setComaddress(String comaddress) {
        this.comaddress = comaddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return Objects.equals(id, company.id) &&
                Objects.equals(comname, company.comname) &&
                Objects.equals(comaddress, company.comaddress);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, comname, comaddress);
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", comname='" + comname + '\'' +
                ", comaddress='" + comaddress + '\'' +
                '}';
    }
}
