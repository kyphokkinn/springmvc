package com.javainterviewpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class CompanyController {
    @Autowired
    private CompanyDAO companyDAO;

    @RequestMapping(value = "/company", method = RequestMethod.POST)
    public ModelAndView saveCompany(@ModelAttribute("company") Company company) {
        if (company.getId() != null) {
            try {
                if (companyDAO.getCompanyById(company.getId()) != null) ;
                companyDAO.updateCompany(company);
            } catch (EmptyResultDataAccessException e) {
                companyDAO.saveCompany(company);
            }
        }
        return new ModelAndView("redirect:/companies");
    }

    @RequestMapping(value = "/editcompany/{id}")
    public ModelAndView editCompany(@ModelAttribute("edit") Company company, @PathVariable("id") int id) {
        ModelAndView model = new ModelAndView("companies");
        company = companyDAO.getCompanyById(id);
        List<Company> companyList = companyDAO.getAllCompanies();
        model.addObject("company", company);
        model.addObject("companyList", companyList);

        return model;
    }

    @RequestMapping(value = "/deletecompany/{id}")
    public ModelAndView deleteCompany(@ModelAttribute("company") Company company, @PathVariable("id") int id) {
        companyDAO.deleteCompany(id);
        return new ModelAndView("redirect:/companies");
    }

    @RequestMapping(value = "/companies")
    public ModelAndView listCompanies(@ModelAttribute("company") Company company) {
        ModelAndView model = new ModelAndView("companies");
        List<Company> companyList = companyDAO.getAllCompanies();
        model.addObject("companyList", companyList);
        return model;
    }
}
