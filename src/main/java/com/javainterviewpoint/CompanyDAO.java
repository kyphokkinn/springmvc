package com.javainterviewpoint;

import java.util.List;

public interface CompanyDAO {
    public void saveCompany(Company company);
    public Company getCompanyById(int id);
    public void updateCompany(Company company);
    public void deleteCompany(int id);
    public List<Company> getAllCompanies();

}
