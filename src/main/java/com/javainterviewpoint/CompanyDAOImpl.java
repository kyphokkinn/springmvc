package com.javainterviewpoint;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
@Repository
public class CompanyDAOImpl implements CompanyDAO{

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Save company to database (id, comname, comaddress)
     * @param company
     */
    @Override
    public void saveCompany(Company company) {
        String sql = "insert into company values(?,?,?)";
        System.out.println("CompanyDAOImpl called");
        jdbcTemplate.update(sql, new Object[]{company.getId(), company.getComname(), company.getComaddress()});
    }

    /**
     * get company from database by id
     * @param id
     * @return compay
     */

    @Override
    public Company getCompanyById(int id) {
        String sql = "select * from Company where id=?";
        Company company = (Company) jdbcTemplate.queryForObject(sql, new Object[]{id}, new RowMapper<Company>() {
            @Override
            public Company mapRow(ResultSet rs, int i) throws SQLException {
                Company company = new Company();
                company.setId(rs.getInt("ID"));
                company.setComname(rs.getString("COMNAME"));
                company.setComaddress(rs.getString("COMADDRESS"));
                return company;
            }
        });
        return company;
    }

    /**
     * update comname comaddress by id
     * @param company
     */
    @Override
    public void updateCompany(Company company) {
        String sql = "update Company set COMNAME = ?, COMADDRESS = ? where id = ?";
        jdbcTemplate.update(sql, new Object[]{company.getComname(), company.getComaddress(), company.getId()});
    }

    /**
     * delete company by id
     * @param id
     */
    @Override
    public void deleteCompany(int id) {
        String sql = "delete from Company where id = ?";
        jdbcTemplate.update(sql, new Object[]{id});
    }

    /**
     * get all company from database
     * @return list of company
     */
    @Override
    public List<Company> getAllCompanies() {
        String sql = "select * from Company";
        List<Company> companyList = jdbcTemplate.query(sql, new ResultSetExtractor<List<Company>>() {
            @Override
            public List<Company> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                List<Company> list = new ArrayList<Company>();
                while (resultSet.next()){
                    Company company = new Company();
                    company.setId(resultSet.getInt("ID"));
                    company.setComname(resultSet.getString("COMNAME"));
                    company.setComaddress(resultSet.getString("COMADDRESS"));
                    list.add(company);
                }
                return list;
            }
        });
        return companyList;
    }
}
