<%--
  Created by IntelliJ IDEA.
  User: ppsdev
  Date: 6/20/2018
  Time: 12:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Insert title here</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>

<div style="width: 1100px; margin: auto;">
    <h3>Add / Edit Company</h3>

    <form:form method="post" action="/company" commandName="company">
        <div class="table-responsive">
            <table class="table table-bordered" style="width: 100%">
                <tr>
                    <td>Id :</td>
                    <td><form:input type="text" path="id" /></td>
                </tr>
                <tr>
                    <td>Name :</td>
                    <td><form:input type="text" path="comname" /></td>
                </tr>
                <tr>
                    <td>Address :</td>
                    <td><form:input type="text" path="comaddress" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input class="btn btn-primary btn-sm" type="submit" value="Submit" /></td>
                </tr>
            </table>
        </div>
    </form:form>
    <br>
    <br>
    <h3>List of Company</h3>
    <table class="table table-bordered" style="width: 100%">
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Address</th>
            <th>Edit/Delete</th>
        </tr>
        <c:if test="${!empty companyList}">
            <c:forEach items="${companyList}" var="company">

                <tr>
                    <td width="60" align="center">${company.id}</td>
                    <td width="60" align="center">${company.comname}</td>
                    <td width="60" align="center">${company.comaddress}</td>
                    <td width="60" align="center">
                        <a href="/editcompany/${company.id}">Edit</a>
                        |
                        <a href="/deletecompany/${company.id}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
                    </td>
                </tr>
            </c:forEach>
        </c:if>
    </table>
</div>
</body>
</html>